var mongoose = require('mongoose');

module.exports = function(){
    function connect() {
        mongoose.connect('mongodb://localhost/startupcampus', function(err) {
            if (err){
                console.error('mongodb connection error', err);
                throw err;
            }
            console.log('mongodb connected');
        });
    }
    connect(); // 연결
    mongoose.connection.on('disconnected', connect); // 연결 끊어졌을 경우 다시 연결 시도
};
