// User Schema (디바이스)
//
/*
 * id - string, 필수, 고유값
 */
var mongoose = require("mongoose");
var UserSchema =  new mongoose.Schema({
    id:{type : String, required:true, unique:true}
});
module.exports = mongoose.model("User", UserSchema);
