// RecommendOrder Schema (주차면 정보 - 실시간용) 

/*
 * floor - String (층 (ex: "B1"))
 * order - Number (ex: 1,2,3, ...) 
 * set_id - String (ex: "Floor_B1_Gate_G1_Kinds_NORMAL_004")
 * gate - gate 정보
 * : gate_name - String (gate 이름)
 * : gate_pos - Number (gate 좌표 (ex: [1,4]))
 * ordered_spots - 배열 
 * : spot_id - String (주차면ID (ex: Floor_b1_number_23))
 * : pos - Number (주차면 좌표 (ex: [4,55]))
 * is_full - Boolean : 해당 set 빈자리여부(빈자리있음/없음)
 * kinds - String : 주차면 종류 ("NORMAL", "HANDI", "ELEC"
 */

var mongoose = require('mongoose');
var RecommendOrderSchema = new mongoose.Schema({
    floor:{type : String, require : true},
    order:{type : Number, require : true},
	set_id: {type : String, require: true, unique: true},
    gate:{ 
        gate_name: {
            type: String, 
            require: true
        },

        gate_pos:[Number]
    },
    ordered_spots:[{
        spot_id: String,
        pos: [Number],
        _id: false
    }],
    is_full:{type : Boolean, require : true},
    kinds: {type: String, require: true}
});
module.exports = mongoose.model("RecommendOrder", RecommendOrderSchema);
