// SpotCollection Schema (주차면 정보 - 데이터 축적) 

/*
 * id - String (ex: Floor_B1_Number_23) 
 * client_id - String  : 주차면에 주차한 사용자 ID
 * floor - String      : 층 (B1/B2)
 * number - Number     : 주차면 번호
 * status - String : 주차유무 ("EMPTYAREA" / "PARKING" / "LONGPARKING" / "RECOMMENDED" / "ERRORBLOCK")
 */

var mongoose = require('mongoose');
var SpotCollectionSchema = new mongoose.Schema({
    id:{type : String, require : true},
    client_id:{type : String},
    floor:{type : String, require : true},
    number:{type : Number, require: true},
    status:{type : String, require: true},
},
{
    timestamps: true
});
module.exports = mongoose.model("SpotCollection", SpotCollectionSchema);
