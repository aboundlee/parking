// Parkingspot Schema (주차면 정보 - 실시간용) 

/*
 * id - String (ex: Floor_B1_Number_23) 
 * client_id - String  : 주차면에 주차한 사용자 ID
 * floor - String      : 층 (B1/B2)
 * number - Number     : 주차면 번호
 * status - String : 주차유무 ("EMPTYAREA" / "PARKING" / "LONGPARKING" / "RECOMMENDED" / "ERRORBLOCK")
 * set_ids : 해당 주차면이 포함된 set의 id
 */

var mongoose = require('mongoose');
var ParkingspotSchema = new mongoose.Schema({
    id:{type : String, require : true, unique : true },
    client_id:{type : String},
    floor:{type : String, require : true},
    number:{type : Number, require: true},
    status:{type : String, require: true},
    set_ids: [{set_id:String, _id: false}]
});
module.exports = mongoose.model("Parkingspot", ParkingspotSchema);
