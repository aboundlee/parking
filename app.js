// 기본 모듈
var express = require('express');
var http = require('http');
var path = require('path');

// 미들웨어
var logger = require('morgan');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');

var expressErrorHandler = require('express-error-handler');

// routing 
var index = require('./routes/index');
var users = require('./routes/users');
var parking_route= require('./routes/update_parkingdata');
var imagecoordinate = require('./routes/imagecoordinate');
var mobileAPI = require('./routes/mobile/api');



// DB
var db = require('./db.js');




//Server
var app = express();
const port = 3000;


//View engine
app.set('views',path.join(__dirname,'views'));
app.set('view engine','pug');

// DB 연결 실행
db();


//app.use
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));



//라우팅 부분
app.use('/', index);
app.use('/users', users);
app.use('/mobile/api', mobileAPI);
app.use('/image_coordinate',imagecoordinate);
app.use('/parkingdata', parking_route);


// 404 에러 페이지 처리
var errorHandler = expressErrorHandler({
    static: {
        '404': './public/404.html'
    }
});

app.listen(port, function() {console.log('Server start port  ' + port);});

