var path = process.cwd();
var RecommendOrderSchema = require(path + '/DBschema/recommend_order.js');
var db = require(path+ '/db.js');
db();
/* For SmallPart class Sorting by distance */
function parts_compare(a,b){
    const disA = a._distance;
    const disB = b._distance;

    let comparison = 0;
    if (disA>disB) {
        comparison = 1;
    } else if (disA<disB) {
        comparison = -1;
    }
    return comparison;
}

/* 거리 계산 
 * spotA : 좌표 ex) [3,4]
 * spotB : 좌표 ex) [5,6]
 */
function distanceOfTwoPoint(spotA, spotB){
    let x = Math.pow((spotA[0]-spotB[0]),2);
    let y = Math.pow((spotA[1]-spotB[1]),2);

    return Math.sqrt((x+y));
}

/* 변수 받기
 * floor : 층수 ex) 'B1'
 * gate : 출구 좌표 ex) { gate_name:'G1', gate_pos:[1,3] }
 * spots_array : ex) 
 *  [
 *    {spot_id:'Floor_B1_Number_27', pos:[40,450]},
 *    {spot_id:'Floor_B1_Number_24', pos:[10,3]},
 *    {spot_id:'Floor_B1_Number_25', pos:[5,44]},
 *    {spot_id:'Floor_B1_Number_23', pos:[1,3]},
 *    {spot_id:'Floor_B1_Number_29', pos:[50,34]}
 *   ]
 *
 */

exports.setOrderdata = function(floor, gate, spots_array, kinds, callback){
    spots_array.sort(function(a,b) {
        return (distanceOfTwoPoint(gate['gate_pos'],a['pos'])-distanceOfTwoPoint(gate['gate_pos'],b['pos']));
    }); 

    let inc = 0;
    let temp_spots = [];
    for(let i=0; i<spots_array.length; i++){

        temp_spots.push(spots_array[i]);
        
        // 앞에서부터 10개씩 묶어서 DB저장, 마지막 남는것들은 10개 이하여도 저장
        if (((i+1)%10==0)||((i+1)==spots_array.length)){
            let set_id = "Floor_" + floor + "_Gate_"+gate.gate_name+"_Kinds_"+kinds+"_Order_"+inc;
            var orderset = new RecommendOrderSchema({'floor': floor, 'set_id': set_id,'order': inc,'gate':{'gate_name':gate['gate_name'], 'gate_pos':gate['gate_pos']}, 'ordered_spots':temp_spots,'is_full':false,'kinds':kinds});

            orderset.save(function(err){
                if (err){
                    console.log(err);
                }
                else {
                    console.log("save");
                }
            });
            inc+=1;
            temp_spots = [];
        }
    }
};
