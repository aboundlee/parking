var path = process.cwd();
var RecommendOrderSchema = require(path + '/DBschema/recommend_order.js');
var ParkingspotSchema = require(path + '/DBschema/parkingspot.js');
/*
var db = require(path+ '/db.js');
db();


let floor = 'B1';
let gate = 'G1';
*/
exports.recommend = function(gate, floor, kinds, callback){

// order 0 부터 빈자리 탐색

	// 해당 층, 게이트에 비어있는 set 탐색
	RecommendOrderSchema.find({"floor": floor, "gate.gate_name": gate, "kinds":kinds, "is_full": false }).sort({"order":1}).exec( function(err, result) { 

		if(err) {callback({"result":"err","parkingID":""}); throw err;}

		// 꽉 찬 경우
		if (result.length == 0) {callback({"result":"full","parkingID":""});}
        
        // 빈 set이 있는 경우 
        else{

            // 가장 우선순위 높은 것 find_set에 저장
            let find_set = result[0].ordered_spots;
            console.log(result);
            console.log("\n");
            console.log("\n");

            spot_loop(find_set,callback);
        }
	});
};

spot_loop = function(find_set,callback){

    let s_id = find_set[0].spot_id;
    console.log(find_set);
            console.log("\n");
            console.log("\n");
// set 안에 있는 주차면 순차 순회 
// 주차면 비어있는지 확인 후 가장 가까운 주차면 ID callback
    ParkingspotSchema.find({"id": s_id}, function(err, data){
        console.log(data);
            console.log("\n");
            console.log("\n");

        if (data[0].status == "EMPTYAREA"){ 
            callback({"result":"success","parkingID":s_id});
        }	
        else {
            find_set.shift();
            spot_loop(find_set,callback);
        }
    });
};
