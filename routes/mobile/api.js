var express = require('express');
var router = express.Router();
var path = process.cwd(); // 루트 경로
var ParkingspotSchema = require(path + '/DBschema/parkingspot.js');
var UserSchema = require(path + '/DBschema/user.js');
var algorithm= require(path + '/algorithm/recommend.js');
/*

1. 사용자 등록 확인 및 등록
2. 실시간 주차면 상태 조회
3. 추천 주차면
4. 주차 위치 등록
5. 주차 위치 확인
6. 주차 위치 등록 해제

*/



/*============================
   사용자 등록 확인 및 등록
=============================*/

router.post('/addUser', function(req,res,next) {
    
    const ClientUuid = req.body.uuid;
    UserSchema.find({id:ClientUuid}, function(err,result){
        // 사용자검색 실패
        if (err) {res.send({'result':'found_fail'}); throw err;}
        // 등록된 사용자
        if (result.length!=0) {res.send({'result':'ok'});}
        else {
            const userDB = {id:ClientUuid};
            UserSchema.create(userDB, function(err, masters){
                // 새로운 사용자 등록 실패
                if (err) {res.send({'result':'regist_fail'});throw err;}
                // 새로운 사용자 등록 완료
                res.send({'result':'regist_ok'});
            });
        }
    });
});


/*=========================
     실시간 주차면 상태
==========================*/
router.post('/status', function(req, res, next) {
  //주차면이 꽉찼을때

    const Floor = req.body.floor;
  // 실제 데이터
    ParkingspotSchema.find({"floor":Floor},{"_id":false, "id":true,"status":true}, function(err,result) {
        if(err) {throw err;}
        res.send(result);
    });
});


/*====================
      추천 주차면
=====================*/
router.post('/recommendSpace', function(req,res,next) {

    const ClientUuid = req.body.uuid;
    const Floor = req.body.floor;
    const Gate = req.body.gate;
    const Kinds = req.body.kinds;

    UserSchema.find({id:ClientUuid}, function(err, result){
        // 사용자 검색 실패
        if (err) {res.send({'result':'found_fail'}); throw err;}
        // 등록되지 않은 사용자
        if (result.length==0) {res.send({'result':'not_user'});}
        else {

            algorithm.recommend(Gate, Floor, Kinds, function(recomresult){
                console.log(recomresult);
                // 검색 실패(에러)
                if (recomresult.result == 'err') {res.send({'result':'fail1'});}
                // 검색 실패(꽉 찬 경우)
                else if (recomresult.result == 'full') {res.send({'result':'fail2'});}
                // 검색 성공
                else if(recomresult.result == 'success') {
                    res.send({'result':'success','parkingID':recomresult.parkingID});
                    
                    // RECOMMENDED로 DB status 업데이트 
                    ParkingspotSchema.update({id: recomresult.ParkingID}, {$set:{status:"RECOMMENDED",client_id:ClientUuid}}, function(err2, result2){
                        if (err2){console.log("update error");}
                    });
                }
                else { res.send({"result":"fail3"});}
            });
        }
        // 주차면이 꽉 찼을 때
    });
});


/*========================
       주차 위치 등록
=========================*/
router.post('/registParkingPos', function(req,res,next) {
    const ClientUuid = req.body.uuid;
    const ClientPos = req.body.spot_id; // 주차면 id
    UserSchema.find({id:ClientUuid}, function(err, result){
        // 사용자 검색 실패
        if (err) {res.send({'result':'found_fail'}); throw err;}
        // 등록되지 않은 사용자
        if (result.length==0) {res.send({'result':'not_user'});}
        else {
            ParkingspotSchema.find({client_id:ClientUuid}, function(err2, result2){

                // 중복 주차 검색 실패
                if (err2){res.send({'result':'duplicate_fail'}); throw err2 }; 
                // 중복 주차
                if ((result2.length!=0)&&(result2[0].status!="RECOMMENDED")) {res.send({'result':'duplicate_regist'});}
                else {
                    ParkingspotSchema.find({"id":ClientPos}, function(err3, result3){
                        // 주차면 id 오류
                        if (err3){res.send({'result':'spotID_fail'}); throw err3;}
                        // 주차면 id 검색 실패 
                        if (result3.length==0) {res.send({'result':'not_exist_spotID'});}
                        else {

                            // RECOMMENDED 상태 데이터, 고객 ID 주차면DB에서 제거(추천 주차면이 아닌 곳에 주차할 수도 있으므로)
                            ParkingspotSchema.update({client_id:ClientUuid},{$set:{client_id:"",status:"EMPTYAREA"}}, function(err4, result5){
                                        if (err4) {res.send("error"); throw err4;}
                            });
                            
                            ParkingspotSchema.update({"id":ClientPos},{$set:{client_id:ClientUuid}}, function(err4, result5){
                                // 등록 실패
                                if (err4) {res.send({'result':'fail'}); throw err4;}
                                // 등록 성공
                                res.send ({'result':'success'});
                            });
                        }
                    });
                }
            });
        }
    })
});

/*=========================
       주차 위치 확인
=========================*/
router.post('/myParkingPos', function(req,res,next) {
    const ClientUuid = req.body.uuid;
    ParkingspotSchema.find({client_id:ClientUuid},{'_id':false, 'id':true, 'client_id':true, 'floor':true}, function(err, resultData){
        // 실패 (주차 위치 등록 안됨)
        if (err) {res.send({'result':'fail'}); throw err;}
        // 실패
        if (resultData.length==0) {res.send({'result':'fail', 'reason':'not_found_user'});}
        // 성공
        else {res.send({'result':'success', 'spot_id':resultData[0].id});}
    });
});



/*=========================
    주차 등록 해제
=========================*/
router.post('/deleteParkingPos', function(req,res,next) {
    const ClientUuid = req.body.uuid;
    ParkingspotSchema.update({client_id:ClientUuid},{$set:{client_id:""}}, function(err, resultData){
        // 실패
        if (err) {res.send({'result':'fail'}); throw err;}
        // 성공
        res.send({'result':'success'});
    });
});


module.exports = router;
