var express = require('express');
var router = express.Router();
var path = process.cwd(); // 루트 경로
var ParkingspotSchema = require(path + '/DBschema/parkingspot.js');
var UserSchema = require(path + '/DBschema/user.js');
var algorithm= require(path + '/algorithm/recommend.js');
var db = require(path+ '/db.js');
db();
/*

1. 사용자 등록 확인 및 등록
2. 실시간 주차면 상태 조회
3. 추천 주차면
4. 주차 위치 등록
5. 주차 위치 확인
6. 주차 위치 등록 해제

*/



  //주차면이 꽉찼을때


    const ClientUuid = "KKK";
    const ClientPos = "Floor_B1_Number_23"//주차면 id
    UserSchema.find({id:ClientUuid}, function(err, result){
        // 사용자 검색 실패
        if (err) {res.send({'result':'found_fail'}); throw err;}
        // 등록되지 않은 사용자
        if (result.length==0) {res.send({'result':'not_user'});}
        else {
            ParkingspotSchema.find({client_id:ClientUuid}, function(err2, result2){

                // 중복 주차 검색 실패
                if (err2){res.send({'result':'duplicate_fail'}); throw err2 }; 
                // 중복 주차
                if ((result2.length!=0)&&(result2[0].status!="RECOMMENDED")) {res.send({'result':'duplicate_regist'});}
                else {
                    // RECOMMENDED 상태 데이터, 고객 ID 주차면DB에서 제거
                    ParkingspotSchema.update({client_id:ClientUuid},{$set:{client_id:"",status:"EMPTYAREA"}}, function(err4, result5){
                                if (err4) {res.send("error"); throw err4;}
                    });
                    ParkingspotSchema.find({"id":ClientPos}, function(err3, result3){
                        // 주차면 id 오류
                        if (err3){res.send({'result':'spotID_fail'}); throw err3;}
                        // 주차면 id 검색 실패 
                        if (result3.length==0) {res.send({'result':'not_exist_spotID'});}
                        else {
                            ParkingspotSchema.update({"id":ClientPos},{$set:{client_id:ClientUuid}}, function(err4, result5){
                                // 등록 실패
                                if (err4) {res.send({'result':'fail'}); throw err4;}
                                // 등록 성공
                                res.send ({'result':'success'});
                            });
                        }
                    });
                }
            });
        }
    })
