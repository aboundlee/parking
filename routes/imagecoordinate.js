var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = process.cwd() // 루트경로

/* 도면 클릭으로 좌표 얻는 페이지 */
router.get('/', function(req, res, next){
    fs.readFile(path + '/public/imagecoordinate.html',function(err, data){
        res.writeHead(200,{'Content-Type' : 'text/html'});
        res.end(data);
    });
});

/* 좌표 txt로 저장 -> 엑셀 작업으로 쿼리 쉽게 만들기*/
router.post('/', function(req, res){
    
    var point = req.body.point;
    var logger = fs.createWriteStream(path + '/public/text/coordinate.txt', {
        flags: 'a' // 'a' means appending (old data will be preserved)
    });

    console.log(point);
    logger.write(point);
    logger.write('\n');
});

module.exports = router;
