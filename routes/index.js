var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = process.cwd() // 루트경로

/* Get home page */

router.get('/', function(req, res, next){
    res.render('/index', {title: 'Express'});
});

router.get('/startupcampus_img', function(req, res, next){
    fs.readFile(path + '/public/images/startupcampus.png',function(err, data){
        res.writeHead(200,{'Content-Type' : 'text/html'});
        res.end(data);
    });
});

module.exports = router;


