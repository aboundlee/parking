var express = require('express');
var request = require('request');
var router = express.Router();
var path = process.cwd(); // 루트 경로
var ParkingspotSchema = require(path + '/DBschema/parkingspot.js');
var RecommendOrderSchema = require(path + '/DBschema/recommend_order.js');
var SpotCollectionSchema = require(path + '/DBschema/spot_collection.js');
var async = require('async');


/* 2초마다 주차면 데이터 받아오기 */
setInterval(function(){

	/* B1 */
	request({
		url: "http://13.124.91.198:8080/parkingblock/data/1/1/2/-1",
		method: "GET" 
	},function(error, response, body){
    if (body){
        // remove non-printable and other non-valid JSON chars
        s = body.replace(/[\u0000-\u0019]+/g,""); 

        var o = JSON.parse(s);

        let spot_info1 = o[0]["DETAILDATA"];
            let floor1 = "B1";
            update_parkingspot(floor1, spot_info1);
    }
	});

	/* B2 */
	request({
		url: "http://13.124.91.198:8080/parkingblock/data/1/1/2/-2",
		method: "GET" 
	},function(error, response, body){
    if (body){
        // remove non-printable and other non-valid JSON chars
        s = body.replace(/[\u0000-\u0019]+/g,""); 

        var o = JSON.parse(s);

        let spot_info2 = o[0]["DETAILDATA"];
            let floor2 = "B2";
            update_parkingspot(floor2, spot_info2);
    }
	});

}, 2000);

function update_parkingspot(floor, spot_info){
	var tasks = [
		function (callback) {
			for (let val of spot_info){
				let num = Object.keys(val)[0];
				let status = val[num];

				ParkingspotSchema.find({"floor":floor, "number": num}, function(err,result){
					if (err) { throw err; }

					// data 없을 경우 
					else if (result.length==0){
						let spot_id = "Floor_"+floor+"_Number_" + num;
						// 해당 spot이 속해있는 set_list 검색
						RecommendOrderSchema.find({"floor" :floor,"ordered_spots.spot_id":spot_id},{'_id':false,'set_id':true}, function(err2,set_list){
							// DB에 데이터 저장
							ParkingspotSchema.create({"id":spot_id, "client_id":"","floor":floor, "number":num, "status":status, "set_ids":set_list}, function(err3, result3){
								if(err3) {console.log(err3);}
							});
						});
					} 
					// data 있을 경우
					else {
						let spot_id = "Floor_"+floor+"_Number_" + num;

						// DB에 데이터 status 업데이트/ RECOMMENDED일 경우 업데이트 안함
                        if (result[0].status!="RECOMMENDED"){
                            ParkingspotSchema.update({"id":spot_id},{$set:{"status":status}}, function(err3, result3){
                                    if(err3) {console.log(err3);}
                            }); 
                        }

					}
				});
			};
            callback(null);
		},
		function (callback) {
			/* recommend_order의 is_full 결정 */
			// floor에 있는 set_id들 검색
			RecommendOrderSchema.find({"floor" :floor},{"_id":false,"set_id":true}, function(err4, result4){
				for (let one of result4){
					one_set = one["set_id"];
					// 해당 set에 포함된 주차면 중 비어있는 주차면 갯수 count
					ParkingspotSchema.count({"set_ids.set_id":one_set, "status":"EMPTYAREA"}, function(err5, count){
						// 비어있는 주차면이 없는 경우, is_full -> true
						if (count==0){ RecommendOrderSchema.update({"set_id":one_set},{$set:{"is_full":true}});}
						// 비어있는 주차면이 하나라도 있는 경우, is_full -> false
						else { RecommendOrderSchema.update({"set_id":one_set},{$set:{"is_full":false}});}
					});
				}
			});
            callback(null);
		},
        
        /* DB 주차면 데이터 쌓기 */
        function (callback){
            ParkingspotSchema.find({floor:floor},{"id":true,"client_id":true,"floor":true,"number":true,"status":true,"_id":false}, function(err,result){
				for (c of result){
					SpotCollectionSchema.create({id:c["id"],client_id:c["client_id"], floor:c["floor"], number:c["number"], status:c["status"]},function(err2,result2){
						if (err2){console.log(err2);}
					});
				}
            });
        }
	];



	async.series(tasks, function (err, results) {
	});
}

module.exports = router;

