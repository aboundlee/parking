FROM node:6.12.2  
RUN  cd /src; npm install  
WORKDIR /root/parkinglot_startupcampus

EXPOSE 3000  

CMD node app.js  
